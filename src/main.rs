use ws::{WebSocket, Sender, Message};
use enigo::{Enigo, Key, MouseButton, KeyboardControllable, MouseControllable};
use std::sync::{Arc, Mutex};
use std::fs::File;
use std::io::prelude::*;
use clap::Parser;


//TODO: add argument support
//TODO: add u16 mouse movement support alongside the default u8
// const ADDRESS_WS: &str = "192.168.7.102:3012";
// const ADDRESS_HTTP: &str = "192.168.7.102:2020";
const FILE_CLIENT_TEST: &str = "client-test.html";
const FILE_CLIENT_PRO: &str = "client-pro.html";

#[derive(Parser, Debug, Clone)]
#[clap(author, version, about, long_about = None)]
struct Args {
    #[clap(short, long, default_value_t = String::from("127.0.0.1"))]
    ip: String,
    #[clap(short, long, default_value_t = 2020)]
    http_port: u16,
    #[clap(short, long, default_value_t = 3012)]
    ws_port: u16,
}

fn main() {
    let args = Args::parse();
    println!("{:?}", args);
    let cargs = args.clone();
    std::thread::spawn(move || {
        start_http(cargs);
    });
    start_ws(args);
}

fn start_http(args: Args) {
    let addr_http = format!("{}:{}",args.ip, args.http_port);
    let addr_ws = format!("{}:{}",args.ip, args.ws_port);
    println!("Starting HTTP server at {}", addr_http);
    rouille::start_server(addr_http, move |request: &rouille::Request| {
        let url = request.url();
        let mut file = if url == "/test" {File::open(FILE_CLIENT_TEST).unwrap()} else {File::open(FILE_CLIENT_PRO).unwrap()};
        let mut html = String::new();
        file.read_to_string(&mut html).unwrap();
        html = html.replace("%WS_IP%", addr_ws.as_ref());
        rouille::Response::html(html)
    });
}

fn start_ws(args: Args) {
    let enigo = Enigo::new(); //TODO: maybe create a separate enigo for every connection? because right now I think there can only be one user at a time
    let a_enigo = Arc::new(Mutex::new(enigo));
    // Websocket factory
    let ws = WebSocket::new(|output: Sender| {
        // Closure per connection
        println!("Client connected");
        let a_enigo = a_enigo.clone();
        let handler = move |msg: Message| {
            // Closure per message
            let a_enigo = a_enigo.clone();
            let enigo: &mut Enigo = &mut *a_enigo.lock().unwrap();
            if msg.is_binary() && !msg.is_empty() {
                let data: Vec<u8> = msg.into_data();
                if data.len() >= 2 {
                    let op: u8 = data[0];
                    if data.len() >= 3 {
                        println!("Op {:02X} Ex {:02X} Ey {:02X} {:?}", data[0], data[1], data[2], data);
                    } else {
                        println!("Op {:02X} Ex {:02X} {:?}", data[0], data[1], data);
                    }
                    let extra = data[1];
                    if op == 0x01 {
                        enigo.key_click(Key::Raw(extra as u16));
                    } else if op == 0x02 {
                        enigo.key_down(Key::Raw(extra as u16));
                    } else if op == 0x03 {
                        enigo.key_up(Key::Raw(extra as u16));
                    } else if op == 0x04 {
                        enigo.mouse_click(pmb(extra));
                    } else if op == 0x05 {
                        enigo.mouse_down(pmb(extra));
                    } else if op == 0x06 {
                        enigo.mouse_up(pmb(extra));
                    } else if op == 0x07 {
                        let amt = u8i(extra);
                        println!("Amt: {}", amt);
                        enigo.mouse_scroll_y(amt);
                    } else if op == 0x08 {
                        let amt = u8i(extra);
                        println!("Amt: {}", amt);
                        enigo.mouse_scroll_x(amt);
                    } else if op == 0x09 && data.len() >= 3 {
                        let amtx = u8i(extra);
                        let amty = u8i(data[2]);
                        println!("AmtX: {} AmtY: {}", amtx, amty);
                        enigo.mouse_move_relative(amtx, amty);
                    } else if op == 0x0A && data.len() >= 3 {
                        let amtx = u8i(extra);
                        let amty = u8i(data[2]);
                        println!("AmtX: {} AmtY: {}", amtx, amty);
                        enigo.mouse_scroll_x(amtx);
                        enigo.mouse_scroll_y(amty);
                    }
                }
            } else {
                output.send(format!("Invalid message {}", msg)).unwrap();
            }
            Ok(())
        };
        // Return handler
        handler
    }).unwrap();
    let addr_ws = format!("{}:{}",args.ip, args.ws_port);
    println!("Starting WS server at {}", addr_ws);
    ws.listen(addr_ws).unwrap();
    println!("All done.")
}

fn pmb(extra: u8) -> MouseButton {
    let mut ret = MouseButton::Left;
    if extra == 0x01 {
        ret = MouseButton::Right;
    } else if extra == 0x02 {
        ret = MouseButton::Middle;
    }
    ret
}

fn u8i(extra: u8) -> i32 {
    extra as i32 - (u8::MAX / 2) as i32
}

// fn u16i(extra: u16) -> i32 {
//     extra as i32 - (u16::MAX / 2) as i32
// }

// fn make_u16(ein: u8, zwei: u8) -> u16 {
//     return ((ein as u16) << 8) | zwei as u16;
// }